# tapir

A website generator using mustache templates and markdown.

Example website: [giml-lang.org](https://giml-lang.org) / [Source](https://gitlab.com/gilmi/giml-lang.org)
