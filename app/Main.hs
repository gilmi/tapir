module Main where

import Prelude (IO)
import Tapir (run)

main :: IO ()
main = run
