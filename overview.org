* Tapir
A static website generator using templates
** Directory structure
*** ~/pages~
Contains the content of the pages in the website.

The top level will be used for #navigation, and ~home.md~ will be used as the
main page of the website.

The pages have the following format:

#+BEGIN_SRC markdown
---
<toml>
---

<markdown>
#+END_SRC

the toml part holds all of the metadata of the page to be used
by the template it uses (which is also specified there).

The markdown part will be available to the template as ~content~,
translated to html.

Example:

#+BEGIN_SRC markdown
---
title = "My post title"
date = 2021-03-01
author = "Gil"
tags = ["new", "project", "welcome"]
template = "blog-post.stache"
---

This is my website!
#+END_SRC
*** ~/static~
Contains all of the static resources for the website such as
favicon, css, js, images, etc.
*** ~/templates~
Contains mustache template files.

Example:

#+BEGIN_SRC mustache
<stylesheet type="text/css" ref="/static/css/main.css"/>

<header class="main header">
    <h1>{{header}}</h1>
</header>

<nav class="main-navigation">
    <ul class="nav-links">
	    {{#navigation}}
		<li class="nav-link">
		    <a href="{{link}}">{{name}}</a>
		</li>
        {{/navigation}} 
	</ul>
</nav>

<div class="main-container">
    <header class="blog-post-header">
      <h2 class="post-title">{{title}}</h2>
	  <p class="post-author-date">by {{author}} at {{date}}</p>
      <ul class="blog-post-tags">
	    {{#tags}}
		<li class="tag">{{.}}</li>
        {{/tags}} 
	  </ul>
    </header>
	
	<content class="blog-post-content">
	{{content}}
	</content>
</div>
#+END_SRC
