module Error where

import RIO
import Text.RawString.QQ (r)
import Data.Text qualified as T

type Error = Text


invalidFormat :: FilePath -> Either Error a
invalidFormat path = Left $ T.unlines
  [ "*** Error: Invalid format for page: " <> T.pack path
  , [r|Pages should have the following format:
---
<toml>
---

<markdown>
|]
  ]

tryToError :: MonadUnliftIO m => m a -> m (Either Error a)
tryToError =
  fmap (first displaySomeException) . try

displaySomeException :: SomeException -> Text
displaySomeException = (<>) "*** Error: " . T.pack . displayException
