
{- | Common HTML functions
-}


{-# language OverloadedStrings #-}

module Html where

import qualified Lucid as H

----------
-- HTML --
----------

-- | Lucid Html type
type Html = H.Html ()

-- | Basic website template including html, charset, stylesheet, etc.
template :: Html -> Html
template = H.doctypehtml_
