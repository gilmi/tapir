{-# LANGUAGE DataKinds, DeriveGeneric, FlexibleInstances, OverloadedStrings, StandaloneDeriving, TypeOperators #-}

module Opts where

import RIO
import Options.Generic

-- | Tapir CLI command
data Command w
  = Build -- ^ Build a website
    { input :: w ::: FilePath <?> "input dir"
    , output :: w ::: FilePath <?> "output dir"
    , replace :: w ::: Bool <?> "override output directory"
    }
  deriving Generic

instance ParseRecord (Command Wrapped)
deriving instance Show (Command Unwrapped)

-- | Parse cli arguments into @Command@
getOpts :: IO (Command Unwrapped)
getOpts = unwrapRecord "tapir - a static website generator from markdown and mustache templates"
