{- | Handles pages parsing, processing and html generation

-}

module Page where

import RIO
import RIO.FilePath
import Data.List
import Data.Vector qualified as V
import Data.Text qualified as T
import Data.Text.IO qualified as T
import Data.Text.Lazy qualified as TL
import Data.HashMap.Strict qualified as HM
import Text.Microstache qualified as Stache
import CMark qualified
import CMark.Lucid qualified as CMark
import Text.Toml qualified as Toml
import Lucid qualified as H
import Data.Aeson qualified as A
import Data.Aeson.KeyMap qualified as AKM

import Html
import Error
import Types
import Text.Processing

-- * Types and constants

data Page
  = Page
    { path :: FilePath
    , metadata :: HM.HashMap Text A.Value
    , content :: CMark.Node
    }

instance A.ToJSON Toml.Node where
  toJSON = \case
    Toml.VTable v -> A.toJSON v
    Toml.VTArray v -> A.toJSON v
    Toml.VString v -> A.toJSON v
    Toml.VInteger v -> A.toJSON v
    Toml.VFloat v -> A.toJSON v
    Toml.VBoolean v -> A.toJSON v
    Toml.VDatetime v -> A.toJSON v
    Toml.VArray v -> A.toJSON v

templatesDir :: FilePath
templatesDir = "_templates"

reservedDirs :: [FilePath]
reservedDirs = [templatesDir]

-- * Parsing

parsePage :: FilePath -> [Text] -> Either Error Page
parsePage path txt = do
  case txt of
    (isSeparator -> True) : (break isSeparator -> (metadata, _ : content)) -> do
      toml <- bimap (T.pack . show) (fmap A.toJSON) $ Toml.parseTomlDoc "" (T.unlines metadata)
      pure $ Page
        { path = path
        , metadata = toml
        , content = CMark.commonmarkToNode [CMark.optUnsafe] (T.unlines content)
        }
    _ ->
      invalidFormat path

-- * Metadata

-- | Make navigation metadata for pages
makeNavigation :: [Page] -> HashMap Text A.Value
makeNavigation pages =
  HM.singleton "navigation" $ A.Array $ V.fromList $ map snd $ sort $ map
      ( \page ->
        let
          order = maybe (A.Number 999) id $ HM.lookup "order" (metadata page)
        in
          (,) order $ A.object
            [ ( "name"
              , maybe (A.String $ T.pack $ path page) id $ HM.lookup "name" (metadata page)
              )
            , ( "link"
              , A.String $ T.pack $ replaceExtension (path page) "html"
              )
            ]
      ) pages

-- | Add metadata to page
addMetadata :: (HashMap Text A.Value) -> Page -> Page
addMetadata md page =
  page{ metadata = HM.union md $ metadata page }

-- * Html

-- | Converts a @Page@ into HTML by applying and converting a mustache template
--   from the subdirectory 'templates' in the input directory with the page's metadata,
--   and injecting markdown content as Html as a 'content' parameter in that template.
--
--   Exceptions may be thrown if the template does not exist on path or if
--   the template parser runs into errors.
toHtml :: Page -> App Html
toHtml Page{..} = do
  case HM.lookup "template" metadata of
    Nothing ->
      pure $ template
        (CMark.renderNode [CMark.optUnsafe] content)

    Just (A.String templatePath) -> do
      Env{inputDir} <- ask
      temp <- liftIO $ Stache.compileMustacheFile (inputDir </> templatesDir </> T.unpack templatePath)
        `catch`
          ( \case
              (Stache.MustacheParserException err) -> error $ show err
              _ -> error "error"
          )
      let
        article = A.String $ CMark.nodeToHtml [CMark.optUnsafe] content
      pure $ template
        (H.toHtmlRaw $ Stache.renderMustache temp $ A.Object $ AKM.fromHashMapText $
         HM.insert "content" article metadata
        )

    _ ->
      error $ "*** Error: Unexpected value for metadata key 'template' in " <> path

-- * IO

-- | Read a page from file.
--
--   Both parsing errors and IO errors will be returned as Either.
readPage :: FilePath -> RIO Env (Either Error Page)
readPage file = do
  Env{inputDir} <- ask
  let path = inputDir </> file
  inputTextOrErr <- liftIO $ tryToError $ T.readFile path
  pure $ parsePage file . T.lines =<< inputTextOrErr

-- | Write a page to file.
--
--   Errors will be thrown as exceptions.
writePageAsHtml :: Page -> RIO Env ()
writePageAsHtml page = do
  Env{outputDir} <- ask
  html <- toHtml page
  liftIO $ T.writeFile
    (outputDir </> replaceExtension (path page) "html")
    (TL.toStrict $ H.renderText html)
