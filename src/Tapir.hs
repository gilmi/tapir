{-# language QuasiQuotes #-}

module Tapir where

import RIO
import RIO.FilePath
import RIO.Directory
import Data.List (partition)
import Control.Monad.Extra (partitionM)
import System.Process (callProcess)
import Data.Text.IO qualified as T

import qualified Opts
import Types
import Page
import Error

run :: IO ()
run = do
  Opts.getOpts >>= \case
    Opts.Build inputDir outputDir replaceOutputDir ->
      runRIO Env{..} build

build :: RIO Env ()
build = do
  checkOutputDir
  Files{..} <- getFiles
  pages <- foldMap (filterAndReportFailures <=< readPage) markdownFiles
  let
    links = makeNavigation pages
    pages' = map (addMetadata links) pages
  for_ pages' $
    filterAndReportFailures <=< tryToError . writePageAsHtml
  copyFiles otherFiles otherDirs
  liftIO $ T.putStrLn "Done."

getFiles :: RIO Env Files
getFiles = do
  Env{inputDir} <- ask
  (markdownFiles, others) <-
    partition ((==) ".md" . takeExtension) <$> listDirectory inputDir
  (otherDirs, otherFiles) <-
    partitionM (doesDirectoryExist . (</>) inputDir)
      . filter ((`notElem` reservedDirs) . takeBaseName)
      $ others
  pure $ Files{..}

filterAndReportFailures :: Either Text a -> RIO Env [a]
filterAndReportFailures =
  either (const (pure []) <=< liftIO . T.hPutStrLn stderr) (pure . pure)

copyFiles :: [FilePath] -> [FilePath] -> RIO Env ()
copyFiles otherFiles otherDirs = do
  Env{..} <- ask
  liftIO $ for_ (otherFiles <> otherDirs) $ \file ->
    catch
      (copyRecursive (inputDir </> file) (outputDir </> file))
      (\ex -> T.hPutStrLn stderr (displaySomeException ex))

copyRecursive :: FilePath -> FilePath -> IO ()
copyRecursive src dest =
  callProcess "cp" ["-r", src, dest]

checkOutputDir :: RIO Env ()
checkOutputDir = do
  Env{outputDir, replaceOutputDir} <- ask
  when replaceOutputDir $
    removeDirectoryRecursive outputDir
  liftIO $ catch
    (createDirectory outputDir)
    ( \ex -> do
      T.hPutStrLn stderr (displaySomeException ex)
      T.hPutStrLn stderr "    Use the flag '--replace' to override output directory."
      exitFailure
    )
