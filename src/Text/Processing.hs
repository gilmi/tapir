module Text.Processing where

import RIO
import Data.Text qualified as T

isEmptyLine :: T.Text -> Bool
isEmptyLine = T.null . trim

isSeparator :: T.Text -> Bool
isSeparator = (==) "---" . trim

trim :: Text -> Text
trim = T.unwords . T.words
