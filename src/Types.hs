module Types where

import RIO


data Files
  = Files
    { markdownFiles :: [FilePath]
    , otherFiles :: [FilePath]
    , otherDirs :: [FilePath]
    }
  deriving Show

data Env
  = Env
    { inputDir :: FilePath
    , outputDir :: FilePath
    , replaceOutputDir :: Bool
    }

type App = RIO Env
